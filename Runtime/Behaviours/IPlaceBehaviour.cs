using UnityEngine;

namespace Deaven.BuildMode.Behaviours
{
    public interface IPlaceBehaviour
    {
        public void Place(GameObject placeable, Vector3 position);
        public void Place(GameObject placeable);
        public bool CanPlace(GameObject placeable, out Vector3 positionToPlaceAt);
    }
}