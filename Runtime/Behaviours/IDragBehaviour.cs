using UnityEngine;

namespace Deaven.BuildMode.Behaviours
{
    public interface IDragBehaviour
    {
        public Vector3 Drag(GameObject draggable);
    }
}