using UnityEngine;

namespace Deaven.BuildMode.Behaviours
{
    public interface IRotateBehaviour
    {
        public void Rotate(GameObject rotatable);
    }
}