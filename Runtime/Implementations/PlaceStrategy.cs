using Deaven.BuildMode.Behaviours;
using UnityEngine;

namespace Deaven.BuildMode.Implementations
{
    public class PlaceStrategy : IPlaceBehaviour
    {
        [SerializeField] private LayerMask groundLayer;

        public void Place(GameObject placeable, Vector3 position)
        {
            placeable.transform.position = position;
        }

        public void Place(GameObject placeable)
        {
            Vector3 position;
            
            if (!CanPlace(placeable, out position))
                return;
            
            placeable.transform.position = position;
        }

        public bool CanPlace(GameObject placeable, out Vector3 positionToPlaceAt)
        {
            RaycastHit hit;
            
            if (!Physics.Raycast(placeable.transform.position, Vector3.down, out hit, Mathf.Infinity, groundLayer))
            {
                positionToPlaceAt = Vector3.zero;
                return false;
            }
            
            positionToPlaceAt = hit.point;
            
            return true;
        }
    }
}