using Deaven.BuildMode.Behaviours;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Deaven.BuildMode.Implementations
{
    public class DragStrategy : IDragBehaviour
    {
        [SerializeField] private Vector3 offset = new Vector3(0, 2, 0);
        [SerializeField] private LayerMask groundLayer;
        
        public Vector3 Drag(GameObject draggable)
        {
            RaycastHit hit;
#if ENABLE_INPUT_SYSTEM
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
#else
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
#endif
            Vector3 currentPosition;
            
            if (!Physics.Raycast(ray, out hit, Mathf.Infinity, groundLayer))
                return Vector3.zero;

            currentPosition = hit.point + offset;

            draggable.transform.position = currentPosition;

            return currentPosition;
        }
    }
}