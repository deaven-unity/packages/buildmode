using Deaven.BuildMode.Behaviours;
using UnityEngine;

namespace Deaven.BuildMode.Implementations
{
    public class RotateStrategy : IRotateBehaviour
    {
        [SerializeField] private Vector3 axis = new Vector3(0, 1, 0);
        [SerializeField] private float angle = 90;

        public void Rotate(GameObject rotatable)
        {
            rotatable.transform.Rotate(axis, angle);
        }
    }
}