using System;
using Deaven.BuildMode.Behaviours;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Deaven.BuildMode
{
    public class BuildMode : SerializedMonoBehaviour
    {
        [SerializeField] private IDragBehaviour DragBehaviour;
        [SerializeField] private IPlaceBehaviour PlaceBehaviour;
        [SerializeField] private IRotateBehaviour RotateBehaviour;

        [SerializeField]
        private GameObject objectToPlace;
    
        private bool _placementIsActive;

        private static BuildMode Instance;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
            {
                Debug.LogWarning("There should be only one BuildMode");
                Destroy(this);
            }
        }

        private void Update()
        {
            if (!_placementIsActive)
                return;
        
            Drag();
        }

        [Button]
        public virtual void StartPlacement(GameObject prefabToPlace)
        {
            objectToPlace = Instantiate(prefabToPlace, transform.position, Quaternion.identity);
            _placementIsActive = true;
        }
    
        [Button]
        public virtual void ExitPlacement()
        {
            objectToPlace = null;
            _placementIsActive = false;
        }

        [Button]
        public void Place()
        {
            PlaceBehaviour.Place(objectToPlace);
        
            ExitPlacement();
        }

        public void PlaceAt(Vector3 position)
        {
            PlaceBehaviour.Place(objectToPlace, position);
        
            ExitPlacement();
        }

        [Button]
        public void Rotate() => RotateBehaviour.Rotate(objectToPlace);

        private void Drag() => DragBehaviour.Drag(objectToPlace);
    }
}